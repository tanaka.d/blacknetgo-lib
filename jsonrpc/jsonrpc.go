package jsonrpc

import (
	"strings"

	blacknet "gitlab.com/blacknet-ninja/blacknetgo-lib"
	"gitlab.com/blacknet-ninja/blacknetgo-lib/blacknet/options"
)

// Config .
type Config struct {
	Endpoint string
}

// JSONRPC .
type JSONRPC struct {
	Config    *Config
	Request   *Request
	Serialize *Serialize
}

// New jsonrpc
func New(config *Config) *JSONRPC {
	return &JSONRPC{
		Request:   NewRequest(),
		Serialize: NewSerialize(config.Endpoint),
		Config:    config,
	}
}

// GetFees .
func (s *JSONRPC) GetFees(message string) uint64 {
	const minTxFee = 0.001 * 1e8
	if len(message) < 1 {
		return minTxFee
	}
	const normalSize = 184
	messageSize := len([]byte(message))
	total := normalSize + messageSize

	return uint64(minTxFee * (1 + total/1000))
}

// Transfer .
func (s *JSONRPC) Transfer(mnemonic, from, to string, amount, fee uint64, args ...interface{}) *Body {
	form := options.Transfer{
		From:   from,
		To:     to,
		Amount: amount,
		Fee:    fee,
	}
	msg, encrypted := formartArgs(args)
	if msg != "" {
		form.Message = msg
		nfee := s.GetFees(msg)
		if nfee > fee {
			fee = nfee
		}
	}
	if encrypted != -1 {
		form.Encrypted = encrypted
	}
	serializedRes := s.Serialize.Transfer(form)
	if serializedRes.Code != 200 {
		return serializedRes
	}
	signature := blacknet.Signature(mnemonic, serializedRes.Body)

	return s.Request.Get(strings.Join([]string{
		s.Config.Endpoint,
		"/api/v2/sendrawtransaction",
		signature,
	}, "/"))
}

// Lease .
func (s *JSONRPC) Lease(mnemonic, from, to string, amount, fee uint64) *Body {
	form := options.Lease{
		From:   from,
		To:     to,
		Amount: amount,
		Fee:    fee,
	}
	serializedRes := s.Serialize.Lease(form)
	if serializedRes.Code != 200 {
		return serializedRes
	}
	signature := blacknet.Signature(mnemonic, serializedRes.Body)
	return s.Request.Get(strings.Join([]string{
		s.Config.Endpoint,
		"/api/v2/sendrawtransaction",
		signature,
	}, "/"))
}

// CancelLease .
func (s *JSONRPC) CancelLease(mnemonic, from, to string, amount, fee uint64, height uint32) *Body {
	form := options.CancelLease{
		From:   from,
		To:     to,
		Amount: amount,
		Fee:    fee,
		Height: height,
	}
	serializedRes := s.Serialize.CancelLease(form)
	if serializedRes.Code != 200 {
		return serializedRes
	}
	signature := blacknet.Signature(mnemonic, serializedRes.Body)
	return s.Request.Get(strings.Join([]string{
		s.Config.Endpoint,
		"/api/v2/sendrawtransaction",
		signature,
	}, "/"))
}

// WithdrawFromLease .
func (s *JSONRPC) WithdrawFromLease(mnemonic, from, to string, withdraw, amount, fee uint64, height uint32) *Body {
	form := options.WithdrawFromLease{
		Withdraw: withdraw,
		From:     from,
		To:       to,
		Amount:   amount,
		Fee:      fee,
		Height:   height,
	}
	serializedRes := s.Serialize.WithdrawFromLease(form)
	if serializedRes.Code != 200 {
		return serializedRes
	}
	signature := blacknet.Signature(mnemonic, serializedRes.Body)
	return s.Request.Get(strings.Join([]string{
		s.Config.Endpoint,
		"/api/v2/sendrawtransaction",
		signature,
	}, "/"))
}
