package withdrawfromlease

import (
	"bytes"
	"encoding/binary"

	"gitlab.com/blacknet-ninja/blacknetgo-lib/utils"
)

// WithdrawFromLease .
type WithdrawFromLease struct {
	Withdraw uint64
	Amount   uint64
	To       string
	Height   uint32
}

// New new a WithdrawFromLease
func New(withdraw uint64, amount uint64, to string, height uint32) *WithdrawFromLease {
	return &WithdrawFromLease{
		Withdraw: withdraw,
		Amount:   amount,
		To:       utils.PublicKeyToHex(utils.PublicKey(to)),
		Height:   height,
	}
}

// Serialize .
func (s *WithdrawFromLease) Serialize() []byte {
	buf := &bytes.Buffer{}
	// withdraw
	binary.Write(buf, binary.BigEndian, s.Withdraw)
	// amount
	binary.Write(buf, binary.BigEndian, s.Amount)
	// to
	binary.Write(buf, binary.BigEndian, utils.HexToPublicKey(s.To))
	// height
	binary.Write(buf, binary.BigEndian, s.Height)
	return buf.Bytes()
}

// Deserialize .
func Deserialize(arr []byte) *WithdrawFromLease {
	return &WithdrawFromLease{
		Withdraw: binary.BigEndian.Uint64(arr[0:8]),
		Amount:   binary.BigEndian.Uint64(arr[8:16]),
		To:       utils.PublicKeyToHex(arr[16:48]),
		Height:   binary.BigEndian.Uint32(arr[48:]),
	}
}
