package jsonrpc

import (
	"io/ioutil"
	"net/http"
	"net/url"
)

// Body .
type Body struct {
	Code int
	Body string
}

// Request .
type Request struct {
}

// NewRequest new a request
func NewRequest() *Request {
	return &Request{}
}

// PostForm . url.Values{"username": {"username"}, "password": {"password"}}
func (rpc *Request) PostForm(url string, data url.Values) *Body {
	res := &Body{Code: 500}
	resp, err := http.PostForm(url, data)
	res.Code = resp.StatusCode
	if err != nil {
		res.Body = err.Error()
		return res
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		res.Code = 400
		res.Body = err.Error()
		return res
	}
	res.Body = string(body)
	return res
}

// Get .
func (rpc *Request) Get(url string) *Body {
	res := &Body{Code: 500}
	resp, err := http.Get(url)
	res.Code = resp.StatusCode
	if err != nil {
		res.Body = err.Error()
		return res
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		res.Code = 400
		res.Body = err.Error()
		return res
	}
	res.Body = string(body)
	return res
}

func formartArgs(args []interface{}) (string, int) {
	var msg string = ""
	var encrypt int = -1
	if len(args) > 0 {
		if len(args) == 1 {
			switch a := args[0].(type) {
			case string:
				msg = a
			case int:
				encrypt = a
			}
		}
		if len(args) == 2 {
			switch a := args[0].(type) {
			case string:
				msg = a
			case int:
				encrypt = a
			}
			switch b := args[1].(type) {
			case string:
				msg = b
			case int:
				encrypt = b
			}
		}
	}
	return msg, encrypt
}
